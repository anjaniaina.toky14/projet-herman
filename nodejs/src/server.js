// Create express app
var express = require('express');
var app = express();
var db = require('./db/database.js');
var cors = require('cors')

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Server port
var HTTP_PORT = 9000;
// Start server
app.listen(HTTP_PORT, () => {
  console.log('Server running on port %PORT%'.replace('%PORT%', HTTP_PORT));
});
// Root endpoint
app.get('/', (req, res, next) => {
  res.json({ message: 'Ok' });
});

app.get('/api/users', (req, res, next) => {
  var sql = 'select * from user';
  var params = [];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: rows,
    });
  });
});

app.get('/api/user/:id', (req, res, next) => {
  var sql = 'select * from user where id = ?';
  var params = [req.params.id];
  db.get(sql, params, (err, row) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: row,
    });
  });
});

app.post('/api/user/', (req, res, next) => {
  var errors = [];
  if (errors.length) {
    res.status(400).json({ error: errors.join(',') });
    return;
  }
  var data = {
    nom: req.body.nom,
    prenom: req.body.prenom,
    age: req.body.age,
    adresse:req.body.adresse
  };
  var sql = 'INSERT INTO user (nom,prenom,age,adresse) VALUES (?,?,?,?)';
  var params = [data.nom, data.prenom, data.age,data.adresse];
  db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: 'success',
      data: data,
      id: this.lastID,
    });
  });
});

app.put('/api/user/:id', (req, res, next) => {
  var data = {
    nom: req.body.nom,
    prenom: req.body.prenom,
    age: req.body.age,
    adresse:req.body.adresse,
  };
  db.run(
    `UPDATE user set 
         nom = COALESCE(?,nom), 
         prenom = COALESCE(?,prenom), 
         age = COALESCE(?,age),
         adresse =  COALESCE(?,adresse)
         WHERE id = ?`,
    [data.nom, data.prenom, data.age,data.adresse, req.params.id],
    function (err, result) {
      if (err) {
        res.status(400).json({ error: res.message });
        return;
      }
      res.json({
        message: 'success',
        data: data,
        changes: this.changes,
      });
    }
  );
});

app.delete('/api/user/:id', (req, res, next) => {
  db.run('DELETE FROM user WHERE id = ?', req.params.id, function (
    err,
    result
  ) {
    if (err) {
      res.status(400).json({ error: res.message });
      return;
    }
    res.json({ message: 'deleted', changes: this.changes });
  });
});

// Default response for any other request
app.use(function (req, res) {
  res.status(404);
});
