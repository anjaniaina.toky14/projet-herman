import React from 'react';
import './index.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Formulaire from './Components/formulaire';
import ListContainer from './Components/listeContainer';
import Update from './Components/updateUser'

import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';

class App extends React.Component{


    render(){
        return(
          <BrowserRouter>
              <div>
                  <ul className="col-12">
                    <li><Link className="navbar" to="/">Accueil</Link></li>
                    <li><Link className="navbar" to="/liste">Listes</Link></li>
                  </ul>
                  <Switch>
                    <Route path="/" exact component={Formulaire}/> 
                    <Route path="/liste"  component={ListContainer}/>   
                    <Route path="/:id"  component={Update}/>    
                  </Switch>
              </div>
          </BrowserRouter>
        )  
    }
}

export default App;

