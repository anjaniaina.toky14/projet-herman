import React from 'react';
import axios from 'axios';


class Update extends React.Component{
    constructor(props){
        super(props);
        this.state={ 
            nom:'',
            prenom:'',
            age:'',
            adresse:'', 
        }
       
        this.handleChange=this.handleChange.bind(this)
        this.handleSubmit=this.handleSubmit.bind(this)
        this.redirect=this.redirect.bind(this)
    }

    handleChange(event){
       this.setState({
          [event.target.name]:[event.target.value]
      })   
    }
    
    componentDidMount(){
        const id= this.props.match.params.id
        axios.get(`http://localhost:9000/api/user/${id}`)
        .then(res=>{
            console.log(res.data.data)
            this.setState({
               nom: res.data.data.nom,
               prenom:res.data.data.prenom,
               age:res.data.data.age,
               adresse:res.data.data.adresse
            })
        })
        .catch(err=>{
            console.log(err)
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const id= this.props.match.params.id
        const data={
            nom:this.state.nom,
            prenom:this.state.prenom,
            age:this.state.age,
            adresse:this.state.adresse
        }
        axios
        .put(`http://localhost:9000/api/user/${id} `, data)
        .then(res=>{
          console.log(res.data)
          this.setState({
              nom:'',
              prenom:'',
              age:'',
              adresse:''
          })
          }
        )
        .catch(err=>console.log(err));
    }

    redirect() {
        window.location.href="http://localhost:3000/liste"
    }
   
    render(){
        return(
            <div>
                 <br/>
                <br/>
                <br/>
                <h4 
                style=
                {{
                    color:'white', 
                    textAlign:'center'
                }}>
                    Modifier votre  
                    <span className='span'> fiche </span> 
                    en remplissant <br/> cette formulaire
                </h4>
                <br/>
                <h6
                    style=
                    {{ 
                        color:'white', 
                        textAlign:'center'
                    }}>
                    Vous pouvez voir les modifications en allant sur
                    <span className='span'> Listes</span>
                </h6>
                <br/>
                <form 
                    className="formulaireContainer col-4" 
                    onSubmit={(e)=>this.handleSubmit(e)} >
                        <div className='formGroup'>
                            <label>Nom</label>
                            <br/>
                            <input
                                name='nom'
                                className="col-12" 
                                type='text'
                                value={this.state.nom}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <div className='formGroup'>
                            <label>Prénom</label>
                            <br/>
                            <input 
                                name='prenom' 
                                className="col-12" 
                                type='text'
                                value={this.state.prenom}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <div className='formGroup'>
                            <label>Age</label>
                            <br/>
                            <input 
                                name='age' 
                                className="col-12" 
                                type='number'
                                value={this.state.age}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <div className='formGroup'>
                            <label>Adresse</label>
                            <br/>
                            <input 
                                name='adresse'
                                className="col-12" 
                                type='text'
                                value={this.state.adresse}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <br/>
                        <div>
                            
                            <button style={{cursor:'pointeur'}} 
                                type='submit' 
                                className='btn btn-primary col-12'
                                onClick={()=>this.redirect()}
                            >
                                Update
                            </button>
                        </div>
                </form>
            </div> 
        )
    }
}

export default Update;