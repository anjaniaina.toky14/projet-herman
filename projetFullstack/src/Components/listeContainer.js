import React from 'react';
import axios from 'axios';
import { Link,withRouter } from 'react-router-dom';



class ListContainer extends React.Component {
  constructor(props){
    super(props);
    this.state={
      indexMore:null,
      fiche:[],
    }        
    this.plusInfo=this.plusInfo.bind(this)
  }

  plusInfo(index) {
    this.state.indexMore===index?
    this.setState({
      indexMore:null
    }):
    this.setState({
      indexMore:index
  });}

  deleteUser(id){
    axios.delete(`http://localhost:9000/api/user/${id}`)
    .then(res => {
      const data =this.state.fiche;
      const newData=data.filter(function(item){
        return item.id!==id
      })
      console.log(res)
      this.setState({
        fiche: newData,
      })
    })
    .catch(err => console.log(err));
  }

  componentDidMount(){
    axios.get(`http://localhost:9000/api/users`).then(res=>{
      console.log(res);
      this.setState({fiche:res.data.data});
    })
  }

  render(){
    const liste=this.state.fiche;
   
    const listes=liste&&liste.map((item, index)=>{  
      if (this.state.indexMore===index) {
        return (
          <div 
            className="ficheItemContainer" 
            key={index} 
          >    
              <h3>{index+1}.  {item.prenom}</h3>
              <div 
                className="infoSup col-9"
              >
                <p>
                  <span className="label">Nom:</span><br/>
                  <input 
                    readOnly
                    className="input" 
                    type="text" 
                    value={item.nom}
                  />
                </p>
                <p>
                  <span className="label">Prénom:</span><br/>
                  <input 
                    readOnly
                    className="input" 
                    type="text" 
                    value={item.prenom} 
                  />
                </p>
                <p>
                  <span className="label">Age:</span><br/>
                  <input 
                    readOnly
                    className="input" 
                    type="text" 
                    value={item.age} 
                  />
                </p>
                <p>
                  <span className="label">Adresse:</span><br/>
                  <input 
                    readOnly
                    className="input" 
                    type="text" 
                    value={item.adresse} 
                  />
                </p>
              </div>
              <button 
                className='btn btn-info listButton offset-2' 
                onClick={()=>this.plusInfo(index)}
              >
                Less
              </button>
              <button 
                className='btn btn-danger listButton' 
                onClick={this.deleteUser.bind(this,item.id)}
              >
                Delete
              </button>
              <Link className='btn btn-primary listButton' to={`/${item.id}`} >edit</Link>
              <hr />
            </div>
          );
        } else {
          return (
            <div key={index}>
              <div>
                <h3>{index+1}.  {item.prenom}</h3>
              </div>
              <button 
                className='btn btn-info listButton' 
                onClick={()=>this.plusInfo(index)} 
              >
                More
              </button>
              <hr />
          </div>
        );
      }
    });

    return( 
      <div className="listContainer col-5">
        <h1 style={{textAlign:'center'}} >Listes</h1><hr/>
        {listes}
      </div>
    )
  } 
}
 
export default withRouter(ListContainer) ;