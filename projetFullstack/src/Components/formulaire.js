import React from 'react';
import axios from 'axios';

class Formulaire extends React.Component{
    constructor(props){
        super(props);
        this.state={
            nom:'',
            prenom:'',
            age:'',
            adresse:''
        }
       
        this.handleChange=this.handleChange.bind(this)
        this.handleSubmit=this.handleSubmit.bind(this) 
    }

    handleChange(event){
       this.setState({
          [event.target.name]:[event.target.value]
      })   
    }

    handleSubmit(e) {
        e.preventDefault();
        const data={
            nom:this.state.nom,
            prenom:this.state.prenom,
            age:this.state.age,
            adresse:this.state.adresse
        }
        
        axios
        .post('http://localhost:9000/api/user' , data)
        .then(res=>{
          console.log(res.data)
          this.setState({
              nom:'',
              prenom:'',
              age:'',
              adresse:''
          })
          }
        )
        .catch(err=>console.log(err));
    }       
    
           
    render(){
        return(
              <div>
                <br/>
                <br/>
                <br/>
                <h4 
                style=
                {{
                    color:'white', 
                    textAlign:'center'
                }}>
                    Faites-vous un petit  
                    <span className='span'> fiche </span> 
                    en remplissant <br/> cette formulaire
                </h4>
                <br/>
                <h6
                    style=
                    {{ 
                        color:'white', 
                        textAlign:'center'
                    }}>
                    Vous pouvez accéder à votre fiche en cliquant sur
                    <span className='span'> Listes</span>
                </h6>
                <br/>
                <form 
                    className="formulaireContainer col-4" 
                    onSubmit={(e)=>this.handleSubmit(e)} >
                        <div className='formGroup'>
                            <label>Nom</label>
                            <br/>
                            <input
                                name='nom'
                                className="col-12" 
                                type='text'
                                value={this.state.nom}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <div className='formGroup'>
                            <label>Prénom</label>
                            <br/>
                            <input 
                                name='prenom' 
                                className="col-12" 
                                type='text'
                                value={this.state.prenom}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <div className='formGroup'>
                            <label>Age</label>
                            <br/>
                            <input 
                                name='age' 
                                className="col-12" 
                                type='number'
                                value={this.state.age}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <div className='formGroup'>
                            <label>Adresse</label>
                            <br/>
                            <input 
                                name='adresse'
                                className="col-12" 
                                type='text'
                                value={this.state.adresse}
                                onChange={(event)=>this.handleChange(event)}
                            />
                        </div>
                        <br/>
                        <div>
                            <button style={{cursor:'pointeur'}} 
                                type='submit' 
                                className='btn btn-primary col-12'
                            >
                                Enregistrer
                            </button>
                        </div>
                </form>
              </div> 
        )
    }
}

export default Formulaire;